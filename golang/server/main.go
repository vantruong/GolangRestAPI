package main

import (
	"net/http"
	"log"
	."config"
)

func main()  {
	router := initRouter()
	http.Handle("/" , router)
	log.Fatal(http.ListenAndServe(Config["host"], nil))
}
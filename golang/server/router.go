package main

import(
	"mux"
	."controller"
)

func initRouter() *mux.Router{
	router := mux.NewRouter()
	//USER ROUTE

	//Categories
	router.HandleFunc("/categories" ,GetCategoriesHandler).Methods("GET")

	//Packages
	router.HandleFunc("/v1/packages",GetPackagesHandler).Methods("GET")

	//User
	router.HandleFunc("/v1/user" , GetUsersHandler).Methods("GET")
	router.HandleFunc("/v1/user/{user_id:[a-zA-Z0-9]+}" , GetUserHandler).Methods("GET")
	router.HandleFunc("/v1/user",InsertUserHandler).Methods("POST")
	router.HandleFunc("/v1/user/{id:[a-zA-Z0-9]+}",UpdateUserHandler).Methods("PUT")
	router.HandleFunc("/v1/user/{id:[a-zA-Z0-9]+}",RemoveUserHandler).Methods("DELETE")
	router.HandleFunc("/v1/auth",Login).Methods("PUT")
	router.HandleFunc("/v1/user/{id:[a-zA-Z0-9]+}",AddPackages).Methods("POST")

	//Transection
	router.HandleFunc("/v1/transections/{user_id:[a-zA-Z0-9]+}",GetTransectionsOfUser).Methods("GET")
	router.HandleFunc("/v1/abc/xzy/{user_id}/{id}/{key}",GetTransectionsOfUser).Methods("GET")

	return router
}

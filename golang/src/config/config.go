package config

import(
	"io/ioutil"
	"encoding/json"
)

var ROOT string

var Config map[string]string

func init(){

	ROOT = "/home/truongcv/GolangRestAPI/golang/"
	//Load config
	configFile := ROOT + "conf/config.json"
	content, err := ioutil.ReadFile(configFile)
	if err != nil {
		panic(err)
	}
	Config = make(map[string]string)
	err = json.Unmarshal(content, &Config)
	if err != nil {
		panic(err)
	}
}
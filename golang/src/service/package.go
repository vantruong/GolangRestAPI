package service

import "model"

/**
** Get List Package
 */
func GetListPackages() ([]model.Package, error) {
	MPackage := model.NewPackage()
	Packages := []model.Package{}
	err := MPackage.FindPackages(&Packages)
	return Packages, err
}

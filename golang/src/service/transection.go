package service

import (
	"model"
	"time"
	"net/url"
)

func GetListTransections() ([]model.Transection, error) {

	MTransection := model.NewTransection()
	Transections := []model.Transection{}
	err := MTransection.FindAllTransections(&Transections)
	return Transections, err

}

func InsertTransection(user_id string, code string, price int64, expire time.Time, params url.Values) {
	MTransection := model.NewTransection()
	//decoder := schema.NewDecoder()
	//err := decoder.Decode(MTransection, params)
	//
	//if err == nil {
		MTransection.UserId = user_id
		MTransection.Code = code
		MTransection.Expire = expire
		MTransection.Price = price
		MTransection.InsertTran()
	//}else{
	//	fmt.Println("Errorrrrrrrrrrr")
	//}

}

func GetTransections(id string) ([]model.Transection, error)  {
	MTransection := model.NewTransection()
	Trans := []model.Transection{}
	err := MTransection.FindTransectionByUserId(id,&Trans)
	return Trans,err
}

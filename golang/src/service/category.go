package service

import (
	"model"
)

/**
 Get All Category
 */
func GetListCategories() ([]model.Category, error)  {

	Cat := model.NewCategory()
	Cats :=[] model.Category{}
	err := Cat.GetAllCategories(&Cats)
	return Cats,err
}
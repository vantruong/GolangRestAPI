package service

import (
	"net/url"
	"github.com/gorilla/schema"
	"model"
	"fmt"
	"time"
	"strconv"
)

/**
** Get List User
 */
func GetListUsers(skip, limit int) ([]model.User, error) {
	User := model.NewUser()
	Users := []model.User{}
	err := User.FindUsers(skip, limit, &Users)
	return Users, err
}



/**
** Get Total Users
 */
func GetTotalUser() (total int, err error) {
	Users := model.NewUser()
	total, err = Users.TotalUsers()
	return total, err
}

/**
** Insert User
 */
func InsertUser(params url.Values) (*model.User, error, int) {
	var result int
	User := model.NewUser()
	decoder := schema.NewDecoder()
	err := decoder.Decode(User, params)

	if err == nil {
		User.Fullname = params.Get("fullname")
		User.Password = params.Get("password")
		User.Email = params.Get("email")
		User.Phone = params.Get("phone")

		if (params.Get("fullname") != "" && params.Get("password") != "" && params.Get("email") != "") || params.Get("phone") != "" {
			err = User.Insert()
			result = 1
		} else {
			result = 2
		}

	}
	return User, err, result
}

func InsertPackagesForUser(User *model.User, params url.Values, id string) (*model.User, *model.Packages, error, int) {
	Package := model.Packages{}
	var result int
	var per int64
	Package.Code = params.Get("code")
	period := params.Get("period")
	per, _ = strconv.ParseInt(period, 0, 64)
	Package.Period = per

	switch Package.Code {
	case "EPL":
		Package.Name="Premier League"
		break
	case "CPL":
		Package.Name="Champions League"
		break
	case "LG1":
		Package.Name="Ligue 1"
		break
	case "LLG":
		Package.Name="Laliga"
		break
	case "ERL":
		Package.Name="Europa League"
		break
	case "BDL":
		Package.Name="Bundesliga"
		break
	case "SRA":
		Package.Name="Serie A"
		break

	}

	switch  per{
	case 1:

		Package.Expire = time.Now().Add(time.Minute*24*60)
		fmt.Println("SwitchInsertPackages", Package.Expire)
		Package.Price = 100
		break
	case 7:
		Package.Expire = time.Now().Add(time.Minute*24*60*7)
		Package.Price = 500
		break
	case 30:
		Package.Expire = time.Now().Add(time.Minute*24*60*30)
		Package.Price = 1000
		break
	}

	fmt.Println("InsertPackagesForUser", Package.Expire)

	user, _ := GetUserByCode(id, Package.Code)
	if user == nil && (User.CurentPoint < Package.Price) {
		fmt.Println("result 0")
		result = 0
		return User,&Package, nil, result
	} else if user == nil && (User.CurentPoint >= Package.Price) {
		fmt.Println("result 1")
		err := User.InsertPackages(Package)
		u, _ := UpdatePointUser(User, per)
		fmt.Println("InsertPackagesForUser: ", u.CurentPoint)
		result = 1
		return u,&Package, err, result
	} else {
		if User.CurentPoint >= Package.Price {

			switch  per{
			case 1:
				fmt.Println("SwitchUpdatePackages", user.Packages[0].Code)
				Package.Expire = user.Packages[0].Expire.Add(time.Minute * 24 * 60)

				Package.Price = 100
				break
			case 7:
				Package.Expire = user.Packages[0].Expire.Add(time.Minute * 24 * 60 * 7)
				Package.Price = 500
				break
			case 30:
				Package.Expire = user.Packages[0].Expire.Add(time.Minute * 24 * 60 * 30)
				Package.Price = 1000
				break
			}
			fmt.Println("SwitchUpdatePackages After", Package.Expire)
			fmt.Println("result 2")
			err := User.UpdatePackages(Package.Code, Package)
			u, _ := UpdatePointUser(User, per)

			userID,_ :=GetUserById(id)
			fmt.Println("InsertPackagesForUser: ", u.CurentPoint)
			result = 2
			return userID,&Package, err, result
		} else {
			result = 3
			return User,&Package, nil, result
		}

	}

}

/**
** Update User
 */
func UpdateUser(User *model.User, params url.Values) (*model.User, error, int) {
	var result int
	decoder := schema.NewDecoder()
	err := decoder.Decode(User, params)

	if err == nil {
		User.Fullname = params.Get("fullname")
		fmt.Println(params.Get("fullname"))
		User.Email = params.Get("email")
		fmt.Println(params.Get("email"))
		User.Phone = params.Get("phone")
		fmt.Println(params.Get("phone"))
		if params.Get("fullname") != "" || params.Get("email") != "" || params.Get("phone") != "" {
			err = User.Upsert()
			result = 1
		} else {
			result = 2
		}

	}
	return User, err, result
}

func UpdatePointUser(User *model.User, period int64) (*model.User, error) {
	switch  period{
	case 1:
		User.CurentPoint = User.CurentPoint - 100;
		fmt.Println("UpdatePointUser Before: ", User.CurentPoint)
		break
	case 7:
		User.CurentPoint = User.CurentPoint - 500;
		fmt.Println("UpdatePointUser Before: ", User.CurentPoint)
		break
	case 30:
		User.CurentPoint = User.CurentPoint - 2000;
		fmt.Println("UpdatePointUser Before: ", User.CurentPoint)
		break
	}

	err := User.UpdateCurrentPoint()
	fmt.Println("UpdatePointUser After: ", User.CurentPoint)
	return User, err
}

/**
Remove User
 */
func RemoveUser(User *model.User) error {
	err := User.RemoveUser()
	return err
}

func GetUserByEmail(email string) (*model.User, error) {
	User := model.NewUser()
	err := User.FindUserByEmail(email)
	if err != nil {
		return nil, err
	}

	return User, err
}

func GetUserByEmailAndPassword(email string, password string) (*model.User, error) {
	User := model.NewUser()
	err := User.CheckEmailAndPassword(email, password)
	if err != nil {
		return nil, err
	}

	return User, err
}

func GetUserBySocial(email string, oauth_id string) (*model.User, error) {
	User := model.NewUser()
	err := User.CheckLoginSocial(email, oauth_id)
	if err != nil {
		return nil, err
	}

	return User, err

}

func GetUserByCode(id string, code string) (*model.User, error) {
	User := model.NewUser()
	fmt.Println(id)
	fmt.Println(code)
	err := User.FindUserByPackageCode(id, code)
	if err != nil {
		return nil, err
	}

	return User, nil
}

func GetUserByCheckVideoExpire(id string, code string) (*model.User, error) {
	User := model.NewUser()
	fmt.Println(id)
	fmt.Println(code)
	err := User.CheckVideoExpireByPackageCode(id, code)
	if err != nil {
		fmt.Println("Errorrrrrrrrrrrrr: ", err)
		return nil, err
	}

	return User, nil
}

/**
** Get User By Id
 */
func GetUserById(id string) (*model.User, error) {
	User := model.NewUser()
	err := User.FindUserById(id)
	if err != nil {
		return nil, err
	}
	return User, err
}






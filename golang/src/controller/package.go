package controller

import (
	"net/http"
	"model"
	"service"
	"encoding/json"
	"fmt"
)

/**
Handler Get Categories
 */
func GetPackagesHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(CONTENT_TYPE, APPLICATION_JSON)
	ResPackages := model.ResPackage{}
	paks, err := service.GetListPackages()
	if err!=nil {
		ResPackages.Code=CODE_400
		ResPackages.Message=MSG_ERROR
	}else{

		ResPackages.Code=CODE_200
		ResPackages.Message=MSG_OK

	}
	ResPackages.Package=paks
	result,_ := json.Marshal(ResPackages)
	fmt.Fprintf(w,string(result))
}

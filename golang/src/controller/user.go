package controller

import (
	"net/http"
	"mux"
	"logger"
	"service"
	"fmt"
	"encoding/json"
	"model"
)

/**
** Handler Users
 */
func GetUsersHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(CONTENT_TYPE, APPLICATION_JSON)
	skip, limit := paramPage(r)
	resUsers := model.ResUsers{}
	users, err := service.GetListUsers(skip, limit)
	if err != nil {
		resUsers.Code = CODE_400
		resUsers.Message = MSG_ERROR
	} else {
		resUsers.Code = CODE_200
		resUsers.Message = MSG_OK
	}
	resUsers.Data = users
	result, _ := json.Marshal(resUsers)
	fmt.Fprintf(w, string(result))
}

/**
** Handler User By Id
 */
func GetUserHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(CONTENT_TYPE, APPLICATION_JSON)
	vars := mux.Vars(r)
	fmt.Println(vars["user_id"])
	resUser := model.ResUser{}
	user, err := service.GetUserById(vars["user_id"])
	if err != nil {
		resUser.Code = CODE_401
		resUser.Message = MSG_ERROR_401
		logger.Errorln("user not found err : ", err)
	} else {
		resUser.Code = CODE_200
		resUser.Message = MSG_OK
	}
	resUser.Data = user
	result, _ := json.Marshal(resUser)
	fmt.Fprintf(w, string(result))
}

/**
Handler Insert User
 */
func InsertUserHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(CONTENT_TYPE, APPLICATION_JSON)
	resUser := model.ResUser{}
	r.ParseForm()
	u, _ := service.GetUserByEmail(r.FormValue("email"))
	if u == nil {
		user, err, result := service.InsertUser(r.Form)
		if err != nil {
			FprintMessge(CODE_400, MSG_ERROR, w)
		} else if err == nil && result == 1 {
			resUser.Data = user
			resUser.Code = CODE_200
			resUser.Message = MSG_OK
			j, _ := json.Marshal(resUser)
			fmt.Fprint(w, string(j))
		} else {
			FprintMessge(CODE_402, MSG_ERROR_402, w)
		}
	} else {

		FprintMessge(CODE_404, MSG_EMAIL_EXIST, w)
	}

}


/*
Handler Update User
 */
func UpdateUserHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(CONTENT_TYPE, APPLICATION_JSON)

	param := mux.Vars(r)
	u, _ := service.GetUserById(param["id"])
	r.ParseForm()

	user, err, re := service.UpdateUser(u, r.Form)
	if err != nil && re == 1 && u != nil {
		FprintMessge(CODE_400, MSG_ERROR, w)
	} else if re == 2 && u != nil {
		FprintMessge(CODE_402, MSG_ERROR_402, w)
	} else if u != nil {
		resUser := model.ResUser{}
		resUser.Data = user
		resUser.Code = CODE_200
		resUser.Message = MSG_OK

		j, _ := json.Marshal(resUser)
		fmt.Fprint(w, string(j))
	} else {
		FprintMessge(CODE_401, MSG_ERROR_401, w)
	}

}

func RemoveUserHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(CONTENT_TYPE, APPLICATION_JSON)
	param := mux.Vars(r)
	u, _ := service.GetUserById(param["id"])
	if u == nil {
		FprintMessge(CODE_401, MSG_ERROR_401, w)
	} else {
		r.ParseForm()
		err := service.RemoveUser(u)
		if err != nil {
			FprintMessge(CODE_400, MSG_ERROR, w)
		} else {
			FprintMessge(CODE_400, MSG_403, w)
		}
	}

}

func AddPackages(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(CONTENT_TYPE, APPLICATION_JSON)
	ResUser := model.ResUser{}
	param := mux.Vars(r)
	r.ParseForm()
	//user, _ := service.GetUserByCode(param["id"],r.FormValue("code"))

	//if user==nil {
	userID, _ := service.GetUserById(param["id"])

	user,MPackage, _, result := service.InsertPackagesForUser(userID, r.Form,param["id"])

	if result == 0 || result ==3{
		ResUser.Code = CODE_405
		ResUser.Message = MSG_POINT_NOT_ENOUGH
	} else if result==1 {
		service.InsertTransection(param["id"],MPackage.Code,MPackage.Price,MPackage.Expire,r.Form)
		GetUserHandler(w, r)
		return
	}else if result ==2{
		ResUser.Data=user
		ResUser.Code = CODE_200
		ResUser.Message = MSG_OK
	}

	//}else{
	//	fmt.Println("code exist")
	//	ResUser.Data=user
	//	ResUser.Code=CODE_200
	//	ResUser.Message=MSG_OK
	//}

	j, _ := json.Marshal(ResUser)
	fmt.Fprintf(w, string(j))
}

func Login(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(CONTENT_TYPE, APPLICATION_JSON)
	resUser := model.ResUser{}
	r.ParseForm()
	userAccount, _ := service.GetUserByEmailAndPassword(r.FormValue("email"), r.FormValue("password"))
	userSocial, _ := service.GetUserBySocial(r.FormValue("email"), r.FormValue("oauth_id"))
	if userAccount == nil && userSocial == nil {
		resUser.Code = CODE_401
		resUser.Message = MSG_ERROR_401
	} else {
		resUser.Data = userAccount
		resUser.Code = CODE_200
		resUser.Message = MSG_LOGIN_SUCCESS
	}

	j, _ := json.Marshal(resUser)
	fmt.Fprintf(w, string(j))
}

func FprintMessge(code string, err string, w http.ResponseWriter) {
	mes := model.Message{code, err}
	j, _ := json.Marshal(mes)
	fmt.Fprint(w, string(j))
}

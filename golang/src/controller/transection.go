package controller

import (
	"net/http"
	"model"
	"service"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
)

func GetAllTransections(w http.ResponseWriter, r *http.Request)  {
	w.Header().Set(CONTENT_TYPE, APPLICATION_JSON)
	resTransections := model.ResTransection{}
	trans, err := service.GetListTransections()
	if err != nil {
		resTransections.Code = CODE_400
		resTransections.Message = MSG_ERROR
	} else {
		resTransections.Code = CODE_200
		resTransections.Message = MSG_OK
	}
	resTransections.Transection= trans
	result, _ := json.Marshal(resTransections)
	fmt.Fprintf(w, string(result))
}

func GetTransectionsOfUser(w http.ResponseWriter,r *http.Request)  {
	w.Header().Set(CONTENT_TYPE, APPLICATION_JSON)
	vars := mux.Vars(r)
	fmt.Println(vars["user_id"])
	fmt.Println("param:", vars)
	fmt.Println("id: ",vars["user_id"])
	fmt.Println("r", mux.Vars(r))
	resTransections := model.ResTransection{}
	result, _ := json.Marshal(resTransections)
	fmt.Fprintf(w,string(result))

	//resTransections := model.ResTransection{}
	//trans,err :=service.GetTransections(param["userid"])
	//if err != nil {
	//	resTransections.Code = CODE_400
	//	resTransections.Message = MSG_ERROR
	//} else {
	//	resTransections.Code = CODE_200
	//	resTransections.Message = MSG_OK
	//}
	//resTransections.Transection= trans
	//result, _ := json.Marshal(resTransections)
	//fmt.Fprintf(w, string(result))
}
package controller

import (
	"net/http"
	"model"
	"service"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
)

/**
Handler Get Categories
 */
func GetCategoriesHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(CONTENT_TYPE, APPLICATION_JSON)
	ResCats := model.ResCategories{}
	cats, err := service.GetListCategories()
	if err!=nil {
		ResCats.Code=CODE_400
		ResCats.Message=MSG_ERROR
	}else{

		ResCats.Code=CODE_200
		ResCats.Message=MSG_OK

	}
	ResCats.Category=cats
	result,_ := json.Marshal(ResCats)
	fmt.Fprintf(w,string(result))
}

func GetCategoryID(w http.ResponseWriter,r *http.Request)  {
	w.Header().Set(CONTENT_TYPE, APPLICATION_JSON)
	vars := mux.Vars(r)
	fmt.Println(vars["id"])
}


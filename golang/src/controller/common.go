package controller

import (
	"net/http"
	"strconv"
)

func paramPage(r *http.Request) (int, int)  {
	skip, err := strconv.Atoi(r.FormValue("offset"))
	if err != nil{
		skip = 0
	}
	limit, err := strconv.Atoi(r.FormValue("limit"))
	if err != nil{
		limit = 10
	}
	return skip, limit
}

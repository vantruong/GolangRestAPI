package logger

import (
	"os"
	"path/filepath"
	"runtime"
)


func Debugf(format string, args ...interface{}) {
	file, err := os.OpenFile(debugFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0777)
	if err != nil {
		return
	}
	defer file.Close()
	New(file).Printf(format, args...)
}

func Debugln(args ...interface{}) {
	file, err := os.OpenFile(debugFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0777)
	if err != nil {
		return
	}
	defer file.Close()
	// With the file and line number to call
	_, callerFile, line, ok := runtime.Caller(1)
	if ok {
		args = append([]interface{}{"File : ", filepath.Base(callerFile), "Line :", line}, args...)
	}
	New(file).Println(args...)
}

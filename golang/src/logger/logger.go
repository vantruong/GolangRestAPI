package logger

import(
	"config"
	"io"
	"log"
	"os"
	"time"
)

var (
	// Logs File
	infoFile  = config.ROOT + "/log/info.log"
	debugFile = config.ROOT + "/log/debug.log"
	errorFile = config.ROOT + "/log/error.log"
)

type logger struct {
	*log.Logger
}

func init() {
	os.Mkdir(config.ROOT+"/log/", 0777)
}

func Infof(format string, args ...interface{}) {
	file, err := openFile(infoFile)
	if err != nil {
		return
	}
	defer file.Close()
	New(file).Printf(format, args...)
}

func Infoln(args ...interface{}) {
	file, err := openFile(infoFile)
	if err != nil {
		return
	}
	defer file.Close()
	New(file).Println(args...)
}

func Errorf(format string, args ...interface{}) {
	file, err := openFile(errorFile)
	if err != nil {
		return
	}
	defer file.Close()
	New(file).Printf(format, args...)
}

func Errorln(args ...interface{}) {
	file, err := openFile(errorFile)
	if err != nil {
		return
	}
	defer file.Close()
	New(file).Println(args...)
}

func New(out io.Writer) *logger {
	return &logger{
		Logger: log.New(out, "", log.Ltime),
	}
}

func openFile(filename string) (*os.File, error) {
	filename += "-" + time.Now().Format("060102")

	return os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0777)
}
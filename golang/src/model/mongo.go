package model

import(
	"gopkg.in/mgo.v2"
)

type MgoBase struct{}

func (this *MgoBase) Session() *mgo.Session{
	return _Session
}

func (this *MgoBase) Database() *mgo.Database{
	return _Database
}

func (this *MgoBase) Collection(cName string) *mgo.Collection{
	return this.Database().C(cName)
}

func (this *MgoBase) Find(cName string , query , selector interface{}) *mgo.Query{
	return this.Database().C(cName).Find(query).Select(selector)
}

func (this *MgoBase) Count(cName string , query interface{})(int , error){
	return this.Database().C(cName).Find(query).Count()
}
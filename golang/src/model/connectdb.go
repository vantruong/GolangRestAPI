package model

import (
	"fmt"
	"gopkg.in/mgo.v2"
	. "config"
	"logger"
)

var(
	_Session *mgo.Session
	_Database *mgo.Database
)

func init(){
	var err error
	_Session , err = mgo.Dial(fmt.Sprintf("%s" , Config["strMongoConnect"]))

	if err != nil{
		logger.Errorln("can not connect to mongodb : " , err)
		panic(fmt.Sprintf("Initialize mongodb error:%v", err))
		return
	}

	_Database = _Session.DB(Config["mongo_db_name"])

	if _Database == nil {
		logger.Errorln("database found")
		panic("database found")
		return
	}

	if err = _Session.Ping(); err != nil {
		panic(fmt.Sprintf("MongoDB execute ping error:%v", err))
	}
	logger.Debugln("MongoDB initialize success.")
	mgo.SetDebug(true)

}

func Close(){
	_Session.Close()
}
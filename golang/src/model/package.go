package model

type Package struct {
	MgoBase
	Name   string `bson:"name,omitempty" json:"name" schema:"name"`
	Code   string `bson:"code,omitempty" json:"code" schema:"code"`
	Period string `bson:"period,omitempty" json:"period" schema:"period"`
	Price  string `bson:"price,omitempty" json:"price" schema:"price"`
}

type ResPackage struct {
	Code    string `bson:"code,omitempty" json:"code" schema:"code"`
	Message string `bson:"message,omitempty" json:"message" schema:"message"`
	Package [] Package `bson:"data,omitempty" json:"data" schema:"data"`
}

func NewPackage() *Package {
	return &Package{
		MgoBase : MgoBase{}, }
}

/**
** get Collection Package
 */
func (this *Package) GetCollectionPackage() string {
	return "package"
}

/**
** Get Users
 */
func (this *Package) FindPackages(Packages *[]Package) error {
	return this.Find(this.GetCollectionPackage(), nil, nil).All(Packages)
}

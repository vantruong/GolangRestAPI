package model

import (
	"gopkg.in/mgo.v2/bson"
	"time"
	"fmt"
)

type Transection struct {
	MgoBase
	Id        bson.ObjectId `bson:"_id,omitempty" json:"id" schema:"id"`
	UserId    string `bson:"user_id,omitempty" json:"user_id" schema:"user_id"`
	Name      string `bson:"name,omitempty" json:"name" schema:"name"`
	Code      string `bson:"code,omitempty" json:"code" schema:"code"`
	Expire    time.Time `bson:"expire,omitempty" json:"expire" schema:"expire"`
	Price     int64 `bson:"price,omitempty" json:"price" schema:"price"`
	CreatedAt time.Time `bson:"created_at,omitempty" json:"created_at" schema:"created_at"`
	UpdatedAt time.Time `bson:"updated_at,omitempty" json:"updated_at" schema:"updated_at"`
}

type ResTransection struct {
	Code string `bson:"code,omitempty" json:"code" schema:"code"`
	Message string `bson:"message,omitempty" json:"message" schema:"message"`
	Transection [] Transection `bson:"transection,omitempty" json:"transection" schema:"transection"`
}

func NewTransection() *Transection {
	return &Transection{
		MgoBase: MgoBase{},
	}
}

func (this *Transection) InsertTran() error {
	this.CreatedAt=time.Now()
	this.UpdatedAt=time.Now()
	return this.Collection(this.GetCollectionTransection()).Insert(this)
}

func (this *Transection) GetCollectionTransection()  string{
	return "app_transection"
}

func (this *Transection) FindAllTransections(Transections *[]Transection)  error{
	return this.Find(this.GetCollectionTransection(),nil,nil).All(Transections)
}

func (this *Transection) FindTransectionByUserId(id string, Transections *[]Transection) error {
	fmt.Println("ID: ",id)
	return this.Find(this.GetCollectionTransection(), bson.M{"user_id":bson.ObjectIdHex(id)}, nil).All(Transections)
}


package model

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Category struct {
	MgoBase
	Id bson.ObjectId `bson:"_id,omitempty" json:"id" schema:"id"`
	Name string `bson:"name,omitempty" json:"name" schema:"name"`
	NameSlug string `bson:"name_slug,omitempty" json:"name_slug" schema:"name_slug"`
	Description string `bson:"description,omitempty" json:"description" schema:"description"`
	Thumb string `bson:"thumb,omitempty" json:"thumb" schema:"thumb"`
	UpdatedAt time.Time `bson:"updated_at,omitempty" json:"updated_at" schema:"updated_at"`
	CreatedAt time.Time  `bson:"created_at,omitempty" json:"created_at" schema:"created_at"`
}

func NewCategory() *Category  {
	return &Category{
		MgoBase: MgoBase{},
	}
}

type ResCategories struct {
	Code string `json:"code"`
	Message string `json:"message"`
	Category []Category `json:"categories"`
}

func (this *Category) GetColection()  string{
	return "categories"
}

func (this *Category) GetAllCategories(Categories *[]Category)  error {
	return this.Find(this.GetColection(),nil,nil).All(Categories)
}
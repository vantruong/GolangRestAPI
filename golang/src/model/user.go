package model

import (
	"gopkg.in/mgo.v2/bson"
	"time"
	"fmt"
)

/*
** User Struct
 */
type User struct {
	MgoBase
	Id             bson.ObjectId `bson:"_id,omitempty" json:"id" schema:"id"`
	Fullname       string `bson:"fullname,omitempty" json:"fullname" schema:"fullname"`
	Password       string `bson:"password,omitempty" json:"password" schema:"password"`
	Phone          string `bson:"phone,omitempty" json:"phone" schema:"phone"`
	Email          string `bson:"email,omitempty" json:"email" schema:"email"`
	Avatar         string `bson:"avatar,omitempty" json:"avatar" schema:"avatar"`
	AuthorID       string `bson:"oauth_id,omitempty" json:"oauth_id" schema:"oauth_id"`
	AuthorProvider string `bson:"oauth_provider,omitempty" json:"oauth_provider" schema:"oauth_provider"`
	Packages       [] Packages `bson:"packages,omitempty" json:"packages" schema:"packages"`
	CurentPoint    int64 `bson:"current_point,omitempty" json:"current_point" schema:"current_point"`
	LastLogin      string `bson:"last_login,omitempty" json:"last_login" schema:"last_login"`
	CreatedAt      time.Time `bson:"created_at,omitempty" json:"created_at" schema:"created_at"`
	UpdatedAt      time.Time `bson:"updated_at,omitempty" json:"updated_at" schema:"updated_at"`
}

type Packages struct {
	Id         bson.ObjectId `bson:"_id,omitempty" json:"id" schema:"id"`
	Name       string `bson:"name,omitempty" json:"name" schema:"name"`
	Code       string `bson:"code,omitempty" json:"code" schema:"code"`
	Period     int64 `bson:"period,omitempty" json:"period" schema:"period"`
	Expire     time.Time `bson:"expire,omitempty" json:"expire" schema:"expire"`
	Price      int64 `bson:"price,omitempty" json:"price" schema:"price"`
	CategoryId bson.ObjectId `bson:"category_id,omitempty" json:"category_id" schema:"category_id"`
}

type ResUser struct {
	Code    string `json:"code"`
	Message string `json:"message"`
	Data    *User `json:"data"`
}

type ResUsers struct {
	Code    string `json:"code"`
	Message string `json:"message"`
	Data    []User `json:"data"`
}

/**
** Init User
 */
func NewUser() *User {
	return &User{
		MgoBase : MgoBase{},
	}
}

/**
** get Collection User
 */
func (this *User) GetCollection() string {
	return "app_users"
}

/**
** Insert User
 */
func (this *User) Insert() error {
	this.CreatedAt = time.Now()
	this.UpdatedAt = time.Now()
	this.CurentPoint = 1000
	this.Id = bson.NewObjectId()
	return this.Collection(this.GetCollection()).Insert(this)
}

/**
** Update User
 */

func (this *User) Update() error {
	//this.UpdatedAt = time.Now()
	err := this.Collection(this.GetCollection()).Update(bson.M{"_id" : this.Id}, bson.M{"$set" : bson.M{
		"fullname" : this.Fullname,
		"email" : this.Email,
		"phone" : this.Phone,
		"updated_at":time.Now(),

	}})
	return err
}

/**
** Update current point user
 */
func (this *User) UpdateCurrentPoint() error  {
	//this.UpdatedAt=time.Now()
	fmt.Println("User: ",this.CurentPoint)
	err := this.Collection(this.GetCollection()).Update(bson.M{"_id":this.Id},
		bson.M{"$set": bson.M{
			"current_point":this.CurentPoint,
			"updated_at":time.Now()}})
	return err
}

/**
** Upsert User
 */

func (this *User) Upsert() error {
	this.UpdatedAt = time.Now()
	_, err := this.Collection(this.GetCollection()).Upsert(bson.M{"_id" : this.Id}, bson.M{"$set" : bson.M{
		"fullname" : this.Fullname,
		"email" : this.Email,
		"phone" : this.Phone,
		"updated_at":time.Now(),
	}})
	return err
}

func (this *User) InsertPackages(P Packages) error  {
	err := this.Collection(this.GetCollection()).Update(bson.M{"_id" : this.Id}, bson.M{"$addToSet" : bson.M{
		"packages" : P,
		}})

	return err

}

func (this *User) UpdatePackages(code string,P Packages) error{
	fmt.Println("UpdatePackages",P.Expire)
	err := this.Collection(this.GetCollection()).Update(bson.M{"_id":this.Id,"packages.code":code},
	bson.M{"$set": bson.M{"packages.$.expire": P.Expire}})
	return err
}

/**
** Remove User
 */
func (this *User) RemoveUser() error {
	err := this.Collection(this.GetCollection()).Remove(bson.M{"_id":this.Id})
	return err

}

/**
** Get User Detail
 */

func (this *User) FindUserById(id string) error {
	return this.Collection(this.GetCollection()).FindId(bson.ObjectIdHex(id)).One(this)
}

func (this *User) FindUserByEmail(email string) error {
	return this.Collection(this.GetCollection()).Find(bson.M{"email":email}).One(this)
}

/**
** Get Users
 */
func (this *User) FindUsers(skip, limit int, Users *[]User) error {
	return this.Find(this.GetCollection(), nil, nil).Skip(skip).Limit(limit).All(Users)
}

/**
** Get Total Users
 */
func (this *User) TotalUsers() (total int, err error) {
	return this.Count(this.GetCollection(), nil)
}

func (this *User) CheckEmailAndPassword(_email string, _password string) error {

	return this.Collection(this.GetCollection()).Find(bson.M{"email":_email, "password":_password}).One(this)
}

func (this *User)  CheckLoginSocial(_email string, oauth_id string) error {
	return this.Collection(this.GetCollection()).Find(bson.M{"email":_email, "oauth_id":oauth_id}).One(this)
}

func (this *User) FindUserByPackageCode(id string, code string ) error  {
	return this.Find(this.GetCollection(),bson.M{"_id":bson.ObjectIdHex(id),"packages.code":code},
		bson.M{"packages":bson.M{"$elemMatch":bson.M{"code":code}}}).One(this)
}

func (this *User) CheckVideoExpireByPackageCode(id string, code string ) error  {
	fmt.Println("Time now: ", time.Now())
	return this.Find(this.GetCollection(),bson.M{"_id":bson.ObjectIdHex(id),"packages.code":code,"packages.expire":bson.M{"$gt": time.Now()}},
		bson.M{"packages":bson.M{"$elemMatch":bson.M{"code":code, "expire":bson.M{"$gt": time.Now()}}}}).One(this)
}




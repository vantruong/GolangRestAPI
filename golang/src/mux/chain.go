package mux

import (
	"net/http"
)

type FilterChain struct {
	filters []Filter
	cur     int //
}

// New Filter Chain
func NewFilterChain(filters ...Filter) *FilterChain {
	return &FilterChain{filters: filters}
}

// AddFilter
func (this *FilterChain) AddFilter(filter Filter) *FilterChain {
	this.filters = append(this.filters, filter)
	return this
}

// Append the two combined Filter Chain
func (this *FilterChain) Append(filterChain *FilterChain) *FilterChain {
	if len(this.filters) == 0 {
		this.filters = filterChain.filters
	} else {
		this.filters = append(this.filters, filterChain.filters...)
	}
	return this
}

// Run The FilterChain
func (this *FilterChain) Run(handler HandlerFunc, rw http.ResponseWriter, req *http.Request) {
	if this.cur < len(this.filters) {
		i := this.cur
		this.cur++
		if this.filters[i].PreFilter(rw, req) {
			this.Run(handler, rw, req)
			this.filters[i].PostFilter(rw, req)
		} else {
			// 
			this.cur = 0
			// Error process is executed
			this.filters[i].PreErrorHandle(rw, req)
		}
	} else {
		// Reset
		this.cur = 0
		handler(rw, req)
	}
}
